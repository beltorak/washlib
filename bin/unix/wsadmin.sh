#! /bin/bash

SCRIPT_SELF="$(readlink -f "$0")"

# prepare user root
mkdir -p ~/.wsadmin/etc # ssl keystore goes here
export USER_ROOT="$(cd ~/.wsadmin && pwd -P)"

T=$(mktemp -d ${USER_ROOT}/.tmp.$(basename "$0").XXXXXXXX)
trap "rm -rf $T" 0

# parse cmd line args
declare -a SHELL_ARGS
while [ "$1" ]; do
	case "$1" in
		--help)
			echo "Help not available for this launcher script yet"
			exit 0
		;;
		--server)
			shift
			SERVER=$1
		;;
		--*)
			echo "Unrecognized option: $1" >&2
			exit 1
		;;
		*)
			SHELL_ARGS+=( "$1" )
		;;
	esac
	shift
done

# prepare base environment
export WAS_HOME="$(dirname "$(dirname "$SCRIPT_SELF")")"
export JAVA_HOME="$WAS_HOME/java/jre"
export PATH="$WAS_HOME/bin;$JAVA_HOME/bin;$PATH"
export SOAP_URL="file://${WAS_HOME}/properties/soap.client.props"

# add sensible user.root to SSL properties
echo "user.root=${USER_ROOT}" > $T/ssl.client.props
sed -e '/^user\.root\s*=/d' "${WAS_HOME}/properties/ssl.client.props" >> $T/ssl.client.props
export CLIENTSSL_URL="file://$T/ssl.client.props"

# If the --server parameter was given,
if [ "$SERVER" ]; then
	# add username and password to soap properties
	cp "${WAS_HOME}/properties/soap.client.props" "$T/soap.client.props"
	if [ -r "$USER_ROOT/servers/$SERVER.auth.props" ]; then
		cat "$USER_ROOT/servers/$SERVER.auth.props" >> "$T/soap.client.props"
	elif [ -r "$WAS_HOME/properties/servers/$SERVER.auth.props" ]; then
		cat "$WAS_HOME/properties/servers/$SERVER.auth.props" >> "$T/soap.client.props"
	fi
	export SOAP_URL="file://$T/soap.client.props"

	# specify host and port on the command line
	if [ -r "$USER_ROOT/servers/$SERVER.host+port.txt" ]; then
		cat "$USER_ROOT/servers/$SERVER.host+port.txt" | sed -e '/^#/d;/^\s*$/d' > "$T/host+port.txt"
	else
		cat "$WAS_HOME/properties/servers/$SERVER.host+port.txt" | sed -e '/^#/d;/^\s*$/d' > "$T/host+port.txt"
	fi
	read SHELL_ARGV_HOST SHELL_ARGV_PORT < "$T/host+port.txt"
	SHELL_ARGS+=( -host $SHELL_ARGV_HOST -port $SHELL_ARGV_PORT )
fi

SCRIPT_LIBRARIES="${USER_ROOT}/scriptLibraries"
SCRIPT_PACKAGES="${USER_ROOT}/scriptPackages"
[[ "$PYTHONPACKAGES" ]] && SCRIPT_PACKAGES="$SCRIPT_PACKAGES:$PYTHONPACKAGES"

declare -a C_PATH
for dir in lib runtimes plugins; do
	find "$WAS_HOME/$dir" -type f -iname '*.jar' > $T/jars
	while read jar; do
		[[ "$jar" ]] && C_PATH+=( "$jar" )
	done < $T/jars
done

"${JAVA_HOME}/bin/java" \
	-cp "$( echo "${C_PATH[@]}" | tr ' ' ':')" \
	-Dwas.install.root="$WAS_HOME" \
	-Duser.install.root="$USER_ROOT" \
	-Dws.ext.dirs="${JAVA_HOME}/jre/lib/ext" \
	-Djava.util.logging.manager=com.ibm.ws.bootstrap.WsLogManager \
	-Djava.util.logging.configureByServer=true \
	-Dosgi.install.area=${WAS_HOME} \
	-Dosgi.configuration.area=${WAS_HOME}/configuration \
	-Dcom.ibm.SOAP.ConfigURL="${SOAP_URL}" \
	-Dcom.ibm.SSL.ConfigURL="${CLIENTSSL_URL}" \
	-Dcom.ibm.ws.scripting.validationOutput=${USER_ROOT}/logs/validation.log \
	-Dcom.ibm.ws.scripting.traceFile=${USER_ROOT}/logs/wsadmin.log \
	-Dcom.ibm.ws.scripting.traceString=com.ibm.*=all=enabled \
	-Dwsadmin.script.libraries="$SCRIPT_LIBRARIES" \
	-Dpython.path="$SCRIPT_PACKAGES" \
	com.ibm.ws.scripting.WasxShell "${SHELL_ARGS[@]}"

