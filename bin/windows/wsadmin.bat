:: Usage: wsadmin -host -port [-username -password]
@if not defined BATCH_DEBUG @( echo off )
setlocal ENABLEEXTENSIONS

:: shorten the batch debug prompt
set PROMPT=$G$S
:: have to grab these before any "shift" as that affects %0
set SELF_NAME=%~f0
set SELF_SHORTNAME=%~n0
set VERBOSE_LEVEL=0
:: automatically trigger utmost verbosity
if defined BATCH_DEBUG ( set VERBOSE_LEVEL=2 )

:: these are deleted at :end, be sure to quote each filename
set TEMPFILES=

goto begin
:print_help
@echo off
:: use 'echo.' to suppress "Echo is Off" on blank lines and to protect
:: leading whitespace.
echo.
echo.======================================================================
echo.              Help for the wsadmin launcher script
echo.======================================================================
echo.
echo.Basic usage is
echo.wsadmin -host HOST -port PORT [-username USERNAME -password PASSWORD]
echo.
echo.This script launches the WebSphere "thin" application client for 
echo.wsadmin scripting. 
echo.
echo.This script takes options starting with '--'.
echo.
echo.                                    ---- Main Options ----
echo.--help                          Shows this help text.
echo.--help-extra                    Shows help for uncommon options.
echo.
echo.--server NAME                   Specifies a server by name. The host 
echo.                                and port are read from a text file.
echo.                                This also triggers '--auth-props' with
echo.                                the same NAME. Execute
echo.                                "wsadmin --help-server" for details.
echo.
echo.--auth-props NAME               Use an alternate SOAP properties file
echo.                                for authentication details. Execute
echo.                                "wsadmin --help-auth" for details.
echo.
echo.--verbose                       Tells the batch script to be more 
echo.                                informative about the execution.
echo.                                This option is effective as soon as
echo.                                it is encountered.
echo.                                * Use twice for more verbosity.
echo.
echo.In addition to the options, if the environment variable
echo.BATCH_DEBUG is set to any value then this batch file will
echo.print out all commands as they are being executed (i.e:
echo."@echo off" is suppressed).
echo.
echo.Anything not specified above is passed directly to the Java thin 
echo.client. Use "wsadmin -help" to get a list of those options.
echo.
goto end

:print_help_extra
@echo off
echo.
echo.======================================================================
echo.    Help for the wsadmin launcher script - Uncommon Options
echo.======================================================================
echo.
echo.These options are not commonly used. They have sensible defaults.
echo.
echo.--java-opt OPT                  Pass OPT directly to the java 
echo.                                executable. This option may be used 
echo.                                more than once. This does not affect
echo.                                java performance options.
echo.
echo.--script-lib-path PATH          Adds a scripting library path. This
echo.                                option can be used more than once. 
echo.                                The default script library paths are
echo.                                "%%WAS_HOME%%\scriptLibraries" and
echo.                                "%%APPDATA%%\wsdamin\scriptLibraries".
echo.
echo.--python-package-path PATH      Adds a python package path. This
echo.                                option can be used more than once. 
echo.                                The default package paths are
echo.                                "%%APPDATA%%\wsadmin\scriptPackages",
echo.                                followed by 
echo.                                "%%WAS_HOME%%\scriptPackages",
echo.                                followed by the folders in
echo.                                "%%PYTHONPACKAGES%%". Paths specified
echo.                                with this switch are prepended to the 
echo.                                front of that list in the order they 
echo.                                are given.
echo.                                
echo.--trace enabled^|disabled       Enable or disable all tracing
echo.                                (respectively). Default is 'enabled'.
echo.
echo.--trace-string STR              Manually override trace string. The 
echo.                                default is "com.ibm.*=all=enabled".
echo.                                This option overrides '--trace='.
echo.
echo.--trace-path PATH               Change the trace log path. The default 
echo.                                is '%%APPDATA%%\wsadmin\logs').
echo.
echo.--wsadmin-props PROPS           Sets system property
echo.                                "-Dcom.ibm.ws.scripting.wsadminprops"
echo.                                to PROPS.
echo.
echo.--config-consistency-check CHK  Sets the system property
echo.                                "-Dconfig_consistency_check" to CHK.
goto end


:print_help_auth
@echo off
echo.
echo.======================================================================
echo.              Help for wsadmin Authentication Files
echo.======================================================================
echo.
echo.--auth-props NAME               Use an alternate SOAP properties file
echo.                                for authentication details.
echo.
echo.This option allows you to store authentication details in an alternate
echo.properties file. If specified, the file "%%NAME%%.auth.props"
echo.in your application settings folder (%%APPDATA%%\wsadmin\servers) is 
echo.combined with global "%%NAME%%.auth.props" in 
echo."%%WAS_HOME%%\properties\servers", and that is combined with the 
echo.default properties file, 
echo."%%WAS_HOME%%\properties\soap.client.properties".
echo.
echo.Although any settings can be placed in one of these auth files, it 
echo.is only really useful to specify these:
echo.  com.ibm.SOAP.loginSource #set to "", "stdin", or "prompt"
echo.  com.ibm.SOAP.loginUserid
echo.  com.ibm.SOAP.loginPassword
echo.(likewise, if you get SAS/CORBA/RMI-IIOP working, the relevent
echo.properties are as above but with CORBA instead of SOAP).
echo.
echo.The global file (%%WAS_HOME%%\properties\auth.%%NAME%%.props)
echo.can be used to provide a "machine account" login for anyone's use
echo.(or it could prompt for a username and password), and each user 
echo.could override that with their specific account credentials for more 
echo.or different privileges.
echo.
echo.To protect passwords from casual leakage you can use the
echo."wspassword.bat" utility to encrypt it. Of course it can also be used
echo.to decrypt it so don't store sensitive passwords on the file system.
echo.
goto end

:print_help_server
@echo off
echo.
echo.======================================================================
echo.            Help for wsadmin Server Definition Files
echo.======================================================================
echo.
echo.--server NAME                   Specifies a server by name. The host 
echo.                                and port are read from a text file.
echo.                                This also triggers '--auth-props' with
echo.                                the same NAME.
echo.
echo.This option allows you to store the server hostname and port in a file
echo.so you do not have to remember those details. The file must be stored
echo."%%APPDATA%%\servers" or "%%WAS_HOME%%\properties\servers". For a 
echo.server named "wasserver1", there must be a text file in that folder 
echo.named "wasserver1.host+port.txt" containing a single line - the host 
echo.name (or IP), some whitespace, and the port number. A file in the 
echo.user's APPDATA folder will take preference over a file in WAS_HOME.
echo.Any blank lines or lines starting with '#' are ignored.
echo.
echo.NOTE that using this option also implies '--auth-props' with the same
echo."NAME" argument. Execute "wsadmin --help-auth" for details.
echo.
goto end


:begin
@call :verbose 1 "Setting up command line environment"
call "%~dp0.\setupCmdLine.bat"
call :argv_defaults

:: Parse command line arguments
@call :verbose 1 "Parsing command line options"
:argv_loop
if "%1"=="" goto argv_end
set ARGV=%~1
shift
@call :verbose 2 "Examining argument '%ARGV%'"
if "%ARGV:~0,2%"=="--" goto argv_opt_batch
:: an argument to the wsadmin Java class
:argv_wasx_shell
set wasx_args=%wasx_args% "%ARGV%"
:: host and port are required; if you don't have them then
:: WasxShell throws an NPE; thanks IBM :-/
:: 'reset JSPWiki syntax
if "%ARGV%"=="-host" ( set ARGV_HOST_PRESENT=1 )
if "%ARGV%"=="-port" ( set ARGV_PORT_PRESENT=1 )
:: All WasxShell options except help take an argument
if "%ARGV%"=="-help" goto argv_loop
if "%ARGV%"=="-?" goto argv_loop
if not "%ARGV:0,1%"=="-" goto argv_loop
set ARGV=%~1
shift
set wasx_args=%wasx_args% "%ARGV%"
goto argv_loop

:argv_opt_batch
if "%ARGV%"=="--verbose" goto argv_be_verbose
if "%ARGV%"=="--help" goto print_help
if "%ARGV%"=="--help-extra" goto print_help_extra
if "%ARGV%"=="--help-auth" goto print_help_auth
if "%ARGV%"=="--help-server" goto print_help_server
if "%ARGV%"=="--auth-props" goto argv_auth_props
if "%ARGV%"=="--server" goto argv_server_name
if "%ARGV%"=="--java-opt" goto argv_java_opt
if "%ARGV%"=="--script-lib-path" goto argv_add_script_lib_path
if "%ARGV%"=="--python-package-path" goto argv_add_python_package_path
if "%ARGV%"=="--trace" goto argv_trace
if "%ARGV%"=="--trace-string" goto argv_trace_string
if "%ARGV%"=="--trace-path" goto argv_trace_path
if "%ARGV%"=="--wsadmin-props" goto argv_wsadmin_props
if "%ARGV%"=="--config-consistency-check" goto argv_config_consistency_check
:: Not recognized; pass to main Java class
goto argv_wasx_shell

:argv_be_verbose
set /a VERBOSE_LEVEL=VERBOSE_LEVEL+1
@call :verbose %VERBOSE_LEVEL% "Set Verbose Level %VERBOSE_LEVEL%"
goto :argv_loop

:argv_auth_props
call :overlay_auth_props %~1
shift
goto argv_loop

:argv_server_name
set name=%~1
shift
set HAS_ERRORS=
call :set_server_host_and_port %name%
if not "%HAS_ERRORS%"=="" goto :end
call :overlay_auth_props %name%
if not "%HAS_ERRORS%"=="" goto :end
goto argv_loop

:argv_java_opt
set ARGV=%~1
shift
set javaoptions=%javaoptions% "%ARGV%"
goto argv_loop

:argv_add_script_lib_path
set ARGV=%~1
shift
set SCRIPT_LIB_PATHS=%SCRIPT_LIB_PATHS%;%ARGV%
goto argv_loop

:argv_add_python_package_path
set ARGV=%~1
shift
if "%ADDED_PYTHON_PACKAGE_PATHS%"=="" goto set_first_package_path
set ADDED_PYTHON_PACKAGE_PATHS=%ADDED_PYTHON_PACKAGE_PATHS%;%ARGV%
goto end_argv_add_python_package_path
:set_first_package_path
set ADDED_PYTHON_PACKAGE_PATHS=%ARGV%
:end_argv_add_python_package_path
goto argv_loop

:argv_trace
set ARGV=%~1
shift
set wsadminTraceString=
if "%ARGV%"=="enabled" set wsadminTraceString=-Dcom.ibm.ws.scripting.traceString=com.ibm.*=all=enabled
if "%ARGV%"=="disabled" set wsadminTraceString=-Dcom.ibm.ws.scripting.traceString=com.ibm.*=all=disabled
if not "%wsadminTraceString%"=="" goto argv_loop
echo.
echo.***** ERROR
echo.***** Bad option for '--trace'. Must be 'enabled' or 'disabled'.
echo.
goto end

:argv_trace_string
set ARGV=%~1
shift
set wsadminTraceString=-Dcom.ibm.ws.scripting.traceString=%ARGV%
goto argv_loop

:argv_trace_path
set ARGV=%~1
shift
set LOG_FOLDER=%ARGV%
goto argv_loop

:argv_wsadmin_props
set ARGV=%~1
shift
set WSADMIN_PROPERTIES_PROP=-Dcom.ibm.ws.scripting.wsadminprops=%ARGV%
goto argv_loop

:argv_config_consistency_check
set ARGV=%~1
shift
set CONFIG_CONSISTENCY_CHECK_PROP=-Dconfig_consistency_check=%ARGV%
goto argv_loop

:argv_end

:check_required_args
@call :verbose 1 "Checking for Required Arguments"
if "%ARGV_HOST_PRESENT%"=="" ( @echo.***** ERROR: -host is required.& goto end )
if "%ARGV_PORT_PRESENT%"=="" ( @echo.***** ERROR: -port is required.& goto end )

@call :verbose 1 "Setting classpath"
set C_PATH=%WAS_CLASSPATH%
set C_PATH=%C_PATH%;%WAS_HOME%\plugins\com.ibm.ws.security.crypto.jar
set C_PATH=%C_PATH%;%WAS_HOME%\plugins\com.ibm.ws.runtime.client.jar
set C_PATH=%C_PATH%;%WAS_HOME%\runtimes\com.ibm.ws.webservices.thinclient_7.0.0.jar
set C_PATH=%C_PATH%;%WAS_HOME%\runtimes\com.ibm.ws.admin.client_7.0.0.jar
@call :verbose 2 "CLASSPATH: %C_PATH%"

if "%LOG_FOLDER%"=="" set LOG_FOLDER=%APPDATA%\wsadmin\logs
mkdir "%LOG_FOLDER%" 2>NUL
set wsadminTraceFile=-Dcom.ibm.ws.scripting.traceFile=%LOG_FOLDER%\wsadmin.traceout.log
set wsadminValOut=-Dcom.ibm.ws.scripting.validationOutput=%LOG_FOLDER%\wsadmin.valout.log
@call :verbose 1 "Writing logs in %LOG_FOLDER%"

set PYTHON_PACKAGE_PATHS=%DEFAULT_PYTHON_PACKAGE_PATHS%
if not "%ADDED_PYTHON_PACKAGE_PATHS%"=="" set PYTHON_PACKAGE_PATHS=%ADDED_PYTHON_PACKAGE_PATHS%;%PYTHON_PACKAGE_PATHS%

:runcmd
@call :verbose 1 "Starting %MAIN_CLASS%"

@if %VERBOSE_LEVEL% GEQ 2 @( @echo on )
"%JAVA_EXE%" ^
        %javaExtDirs% ^
        "-Dws.ext.dirs=%WAS_EXT_DIRS%" ^
        %javaEndorsedDirs% ^
        %PERFJAVAOPTION% ^
        %WAS_LOGGING% ^
        %javaoptions% ^
        %CONSOLE_ENCODING% ^
        %WAS_DEBUG% ^
        "%CLIENTSOAP%" ^
        "%JAASSOAP%" ^
        "%CLIENTSAS%" ^
        "%CLIENTSSL%" ^
        %WSADMIN_PROPERTIES_PROP% ^
        %WORKSPACE_PROPERTIES% ^
        "-Duser.install.root=%USER_INSTALL_ROOT%" ^
        "-Dwas.install.root=%WAS_HOME%" ^
        "%wsadminTraceFile%" ^
        %wsadminTraceString% ^
        "%wsadminValOut%" ^
        %wsadminConnType% ^
        %wsadminLang% ^
        "-Dwsadmin.script.libraries=%SCRIPT_LIB_PATHS%" ^
        "-Dpython.path=%PYTHON_PACKAGE_PATHS%" ^
        -classpath "%C_PATH%" ^
        %MAIN_CLASS% %wasx_args%

@if %VERBOSE_LEVEL% LSS 2 @( @echo off )

:end
:: http://code-bear.com/bearlog/2007/06/01/getting-the-exit-code-from-a-batch-file-that-is-run-from-a-python-program/
@if defined BATCH_DEBUG @( echo on )
set RC=%ERRORLEVEL%
@call :verbose 2 "Deleting temp files"
if not "%TEMPFILES%"=="" ( del %TEMPFILES% )
@call :verbose 1 "Exiting with code %RC%"
endlocal & exit /b %RC%

:: =================================================================
:: ============================= subs ==============================

:argv_defaults
@call :verbose 1 "Setting default values"
set JAVA_EXE=%JAVA_HOME%\bin\java
set AUTH_PROPS_NAME=
set PERFJAVAOPTION=-Xms256m -Xmx792m -Xj9 -Xquickstart
set wsadminTraceString=-Dcom.ibm.ws.scripting.traceString=com.ibm.*=all=enabled
:: The "standard" value logs to the installation location of the thin client, 
:: which may be under "Program Files" and restricted by the UAC; so change 
:: this to the user's home folder. 'reset JSPWIKI syntax highlighter
set LOG_FOLDER=%APPDATA%\wsadmin\logs

:: The default script libarary path is %WAS_HOME%\scriptLibraries; it is
:: always included. We can add additional ones here, so we add
:: %APPDATA%\wsadmin\scriptLibraries if it exists
set SCRIPT_LIB_PATHS=%APPDATA%\wsadmin\scriptLibraries

:: -host and -port are not provided; you have to specify them on the
:: command line. Don't change the connection type - I think only SOAP
:: is supported. 'reset JSPWIKI syntax highlighter
:: you need to make sure the port number is the server SOAP port number you want to connect to, in this example the server SOAP port is 8879
set wsadminConnType=-Dcom.ibm.ws.scripting.connectionType=SOAP

:: specify what language you want to use with wsadmin; 
:: jacl is deprecated so we use jython
set wsadminLang=-Dcom.ibm.ws.scripting.defaultLang=jython

:: main class
set MAIN_CLASS=com.ibm.ws.scripting.WasxShell

:: I am not sure what these are
set WSADMIN_PROPERTIES_PROP=
set WORKSPACE_PROPERTIES=

:: proper python packages; yay :)
:: These are the defaults for the installation and user
set PYTHON_PACKAGE_PATHS=
:: These are the defaults for the system; just put them in whether or not they exist.
set DEFAULT_PYTHON_PACKAGE_PATHS=%APPDATA%\wsadmin\scriptPackages;%WAS_HOME%\scriptPackages
if defined PYTHONPACKAGES ( set DEFAULT_PYTHON_PACKAGE_PATHS=%DEFAULT_PYTHON_PACKAGE_PATHS%;%PYTHONPACKAGES% )
:: these are added on the command line and must be prepended to the final result in the order specified
set ADDED_PYTHON_PACKAGE_PATHS=

goto :EOF

:: concat all client props files to add authentication details
:overlay_auth_props
set AUTH_PROPS_NAME=%1
@call :verbose 1 "Overlaying %AUTH_PROPS_NAME% auth properties on client props files"
::SET CLIENTSAS=-Dcom.ibm.CORBA.ConfigURL=file:%WAS_HOME%/properties/sas.client.props
::SET CLIENTSOAP=-Dcom.ibm.SOAP.ConfigURL=file:%WAS_HOME%/properties/soap.client.props
call :combine_auth_props CLIENTSAS sas CORBA %AUTH_PROPS_NAME%
call :combine_auth_props CLIENTSOAP soap SOAP %AUTH_PROPS_NAME%
goto :EOF

:combine_auth_props
:: varname = CLIENTSAS | CLIENTSOAP
set varname=%1
:: props_base_name = sas | soap
set props_base_name=%2
:: type = CORBA | SOAP
set type=%3
:: overlay_name - as in {%USERPROFILE%\wsadmin,%WAS_HOME%\properties}\%overlay_name%.auth.props
set overlay_name=%4

set tempfile=%TEMP%\tmp-%SELF_SHORTNAME%.%RANDOM%
@call :verbose 1 "Overlaying %type% auth properties"
@call :verbose 2 "Auth overlay for %varname% at %tempfile%"
set TEMPFILES=%TEMPFILES% "%tempfile%"

set source_file=%WAS_HOME%\properties\%props_base_name%.client.props
@call :verbose 2 "Base file: %source_file%"
type "%source_file%" > "%tempfile%"

set source_file=%WAS_HOME%\properties\servers\%overlay_name%.auth.props
if exist "%source_file%" (
        @call :verbose 2 "Global Overlay: %source_file%"
        type "%source_file%" >> "%tempfile%"
) else (
        @call :verbose 2 "Global Overlay Not Found: %source_file%"
)

set source_file=%APPDATA%\wsadmin\servers\%overlay_name%.auth.props
if exist "%source_file%" (
        @call :verbose 2 "User Overlay: %source_file%"
        type "%source_file%" >> "%tempfile%"
) else ( 
        @call :verbose 2 "User Overlay Not Found: %source_file%"
)

SET %varname%=-Dcom.ibm.%type%.ConfigURL=file:%tempfile:\=/%
goto :EOF

:set_server_host_and_port
set name=%~1
set file=
if exist "%APPDATA%\wsadmin\servers\%name%.host+port.txt" set file=%APPDATA%\wsadmin\servers\%name%.host+port.txt
if "%file%"=="" set file=%WAS_HOME%\properties\servers\%name%.host+port.txt
@call :verbose 2 "Setting host and port from %file%"
if not exist "%file%" ( @echo.***** ERROR: Server File ^(%name%.host+port.txt^) Not Found in "%APPDATA%\wsadmin\servers" or "%WAS_HOME%\properties\servers"& set HAS_ERRORS=1 & goto :EOF )
for /F "tokens=1,2,3*" %%H in ('type "%file%"') do (
        call :do_set_server_host_and_port %%H %%I
)
goto :EOF

:do_set_server_host_and_port
set host=%1
set port=%2
if "%host%"=="" (
        @call :verbose 2 "Skipping blank line"
        goto :EOF
)
if "%host:~0,1%"=="#" (
        @call :verbose 2 "Skipping comment"
        goto :EOF
)

@call :verbose 1 "Obtained host and port from file (%host%:%port%)"
set wasx_args=%wasx_args% -host %host% -port %port%
set ARGV_HOST_PRESENT=1
set ARGV_PORT_PRESENT=1
goto :EOF

:verbose
:: this routine is silent
@echo off
if "%VERBOSE_LEVEL%" LSS "%~1" goto :EOF
set verbose_header=+
set verbose_header_len=1
:verbose_header_loop
if %verbose_header_len% GEQ %1 goto verbose_header_loop_end
set verbose_header=%verbose_header%+
set /a verbose_header_len=verbose_header_len + 1
goto verbose_header_loop
:verbose_header_loop_end
echo.%verbose_header% %~2
@if defined BATCH_DEBUG @( @echo on )
goto :EOF

::End of wsadmin.bat 
