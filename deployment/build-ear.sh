#! /bin/bash

CWD="$(cd "$(dirname "$0")" && pwd -P)"
T="$CWD/$(mktemp -d .tmp.$(basename "$0").XXXXXXXX)"
trap "rm -rf \"$T\"" 0

tar -C app -cf - . | tar -C "$T" -xf -
(
	cd "$T/jee-webapp-1.0"
	zip -rq ../jee-webapp-1.0.war .
	cd ..
	rm -rf jee-webapp-1.0
	zip -rq "$CWD/app.ear" .
)
