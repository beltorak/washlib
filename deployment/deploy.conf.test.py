DEPLOY_OPTS = wash.util.AttrDict({
	'standard': [
		'usedefaultbindings',
		'nopreCompileJSPs',
		'distributeApp',
		'nouseMetaDataFromBinary',
		'nodeployejb',
		'createMBeansForResources',
		'noreloadEnabled',
		'nodeployws',
		'validateinstall warn',
		'noprocessEmbeddedConfig',
		'filepermission .*\.dll=755#.*\.so=755#.*\.a=755#.*\.sl=755',
		'noallowDispatchRemoteInclude',
		'noallowServiceRemoteInclude',
		'asyncRequestDispatchType DISABLED',
		'nouseAutoLink'
	],
	'moduleClassLoader': ModuleClassLoader(parentLast = True),
	'jndiRefs': wash.util.AttrDict({
		'internal': [ SimpleJndiMapping(ref = 'jdbc/progreg', target = 'jdbc/progreg') ],
		'external': [
						SimpleJndiMapping(ref = 'jdbc/progreg', target = 'jdbc/progreg'),
						SimpleJndiMapping(ref = 'jndi/amdcMqConnectionFactory', target = 'jndi/amdcMqConnectionFactory')
					]
	}),
	'securityMapping': wash.util.AttrDict({
		'internal': PrefixedLdapRoleMapping(securityDomain = 'devsub', rolePrefix = 'NAT_PROGREG_')
	}), 
	'applicationClassLoader': ApplicationClassLoader(single = False, parentLast = True)
})

DEPLOYMENTS = [
	DeploymentManager('wassaNetwork', applications = [
		Application('jee-app',
			standardOptions = DEPLOY_OPTS.standard, 
			classLoader = ApplicationClassLoader(single = False, parentLast = True), 
			securityMapping = PrefixedLdapRoleMapping(securityDomain = 'vm-ldap', rolePrefix = 'GBL_MI_'), 
			modules = [
				AllModules(
					appServers = [ AppServer('WAS-SA-1') ],
					webServers = [ WebServer('wassa-web') ],
					classLoader = ModuleClassLoader(parentLast = True), 
					jndiRefs = [ SimpleJndiMapping(ref = 'jdbc/app', target = 'jdbc/local/derby') ] )])
	]),
	DeploymentManager('WASMX-Network', applications = [
		Application('jee-app',
			standardOptions = DEPLOY_OPTS.standard, 
			classLoader = ApplicationClassLoader(single = False, parentLast = True),
			securityMapping = PrefixedLdapRoleMapping(securityDomain = 'vm-ldap', rolePrefix = 'GBL_MI_'),
			modules = [
				AllModules(
					appServers = [ ServerCluster('WASMX-Cluster'), AppServer('SA-Server') ],
					webServers = [ WebServer('wasmx-web') ],
					classLoader = ModuleClassLoader(parentLast = True), 
					jndiRefs = [ SimpleJndiMapping(ref = 'jdbc/app', target = 'jdbc/local/derby') ] )])
	])
	
]
