#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import sys

from wash.compat import *
from wash.deploy import *
from wash.util import trace

"""
Deploys an application to WebSphere.
"""


HELP = """
Usage: wsadmin -f deploy.py [options] [earfile [earfile [...]]]

Options:
    -c file
    --config file
        Use a different config file.
        The default is to look for "deploy.conf.py".
"""

def deployEar(earFilePath, appName = None):
	trace("Looking for container specification matching this environment")
	containers = [ container for container in DEPLOYMENTS if container.matchesCurrentEnvironment() ]
	if (len(containers) != 1):
		raise ValueError("Bad Configuration; found %d matching Containers: %s" % (
				len(containers), "; ".join([ "<%s: %s>" % (container.__class__.__name__, container.name) for container in containers ])))
	container = containers[0]
	if (not container):
		raise ValueError("No container configs match current environment")
	trace("Found container spec: %s" % repr(container))
	
	earFile = wash.deploy.EarFile(earFilePath)
	if (not appName):
		appName = earFile.getAppName()
		trace('Discovered app name from ear')
	
	origAppState = wash.state.getApplicationState(appName)
	trace("Application is %s" % origAppState)
	if (origAppState == 'Running'):
		wash.state.stopApplication(appName)
	if (origAppState in [ 'Running', 'Installed' ]):
		container.uninstallApplication(appName)
	
	container.installApplication(earFile, appName)
	trace("Completed Installation")
	AdminConfig.save()
	wash.util.syncNodes()
	
	if (origAppState in [ 'Absent', 'Running' ]):
		wash.state.startApplication(appName)
	
	trace("FINI!")
#

if (__name__ == '__main__'):
	ears = []
	configFile = "deploy.conf.py"
	skipArgs = 0
	for i in range(len(sys.argv)):
		if (skipArgs):
			skipArgs -= 1
			continue
		# FIXME: the wsadmin script does not implement '--' so we cannot take any options that the BAT file sucks up
		if (sys.argv[i].lower() in [ '+help' ]):
			print(HELP)
			sys.exit(0)
		elif (sys.argv[i] in [ "-c", "--config" ]):
			configFile = sys.argv[i + 1]
			skipArgs = 1
			continue
		else:
			ears.append(sys.argv[i])
	
	if (not ears):
		print("\n*** ERROR: No ears given\n*** Run with '+help'")
		sys.exit(1)
	else:
		trace("Looking for config file: %s" % configFile)
		execfile(configFile)
		trace("Deploying %d ears..." % len(ears))
		for ear in ears:
			try:
				deployEar(ear)
			except:
				print("*** ERROR: Failed to deploy %s: %s" % (ear, ": ".join([ str(item) for item in sys.exc_info()[0:2] ])))
# EOF
