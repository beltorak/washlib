# -*- coding: utf-8 -*-

###############################################################################
# Copyright 2012 Trevor "beltorak" Torrez
# under The Apache License, version 2.0.
#
# See http://www.apache.org/licenses/LICENSE-2.0
# (or the file COPYING in the root of this dist)
# for details.
###############################################################################

"""
Puts the five god objects into sys.modules so as to be accessible to proper python packages.

Borrowed heavily from http://webspherehacks.com/blog/?p=6

This provides the ability to 'import'
the WebSphere administration objects. After this setup has completed,
any module loaded can do things like:
import AdminTask, AdminConfig, AdminControl, AdminApp, Help
print AdminConfig.getid('/Cell:/').

This must executed via execfile (or used as a wsadmin.sh profile script).

A nicer option is to use this as a scripting library to avoid requiring that the
end user pollute a private wsadmin profile script. For example,
if this file is "/home/username/.wsadmin/scriptLibraries/wsadminobjs.py":
	wsadmin -javaopt "-Dwsadmin.script.libraries=/home/username/.wsadmin/scriptLibraries"
	reload(wsadminobjs)
(The god objects are not available during the bootstrap process when the
script libraries are read, so the reload is important.)

Afterwards you can "import AdminConfig" (etc) in your python packages.

And you can get true python packages (not just flat-namespaced modules)
by using the wsadmin flag to set the python.path system property; like
	wsadmin -javaopt "-Dpython.path=/home/username/.wsadmin/scriptPackages" \
		-javaopt "-Dwsadmin.script.libraries=/home/username/.wsadmin/scriptLibraries"
So if you put the following at the __init__.py of your package, the end user
doesn't even have to reload the module themsleves:
	import wsadminobjs
	reload(wsadminobjs)

Tested with WebSphere 7 (jython 2.1).

"""

import sys
for _obj in [ 'AdminApp', 'AdminConfig', 'AdminControl', 'AdminTask', 'Help' ]:
	if (not sys.modules.has_key(_obj)):
		try:
			sys.modules[_obj] = eval(_obj)
		except NameError:
			pass
		except:
			print("wsadmin: ERROR globalizing reference to %s: %s" % (
					_obj, "; ".join([ str(item) for item in sys.exc_info() ])))
		else:
			print("wsadmin: added %s to sys.modules" % _obj)

## EOF
