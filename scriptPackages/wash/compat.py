# -*- coding: utf-8 -*-

###############################################################################
# Copyright 2012 Trevor "beltorak" Torrez
# under The Apache License, version 2.0.
#
# See http://www.apache.org/licenses/LICENSE-2.0
# (or the file COPYING in the root of this dist)
# for details.
###############################################################################


"""
This 'module' declares some basic sanity into the jython 2.1 language

This module is "import *" safe
"""

import java.lang.System as _java_lang_system

try:
	EOL
except NameError:
	EOL = _java_lang_system.getProperty('line.separator')

# http://dbrand666.wordpress.com/2010/04/10/fix3/
# Note that this doesn't satisfy the 'is' operator when
# comparing with "native" "boolean" values
try:
	True and False
except NameError:
	class bool(type(1)):
		def __init__(self, val = 0):
			if (val):
				if (type(val) == type(1) and val != 0):
					type(1).__init__(self, 1)
				elif (type(val) == type("") and val.lower() in [ "true" ]):
					type(1).__init__(self, 1)
				else:
					type(1).__init__(self, 0)
			else:
				type(1).__init__(self, 0)
		def __repr__(self):
			if self:
				return "True"
			else:
				return "False"
		__str__ = __repr__

	bool = bool
	False = bool(0)
	True = bool(1)
