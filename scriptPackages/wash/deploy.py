# -*- coding: utf-8 -*-

###############################################################################
# Copyright 2012 Trevor "beltorak" Torrez
# under The Apache License, version 2.0.
#
# See http://www.apache.org/licenses/LICENSE-2.0
# (or the file COPYING in the root of this dist)
# for details.
###############################################################################


"""
Used to descibe and perform an application deployment.

The primary idea of this module is to use classes to
encapsulate the structure of a deployment, and allow each
class to define how that part of the deployment
configuration is implemented.

For the purposes of this module, a "binding" is an applied "mapping".

"""

import AdminApp

import java
import javax.xml.xpath.XPathFactory

from wash.compat import *

import wash.util

class EarFile(wash.util.SelfHelp):
	"""
	Provides binding information from an EAR file.
	"""

	def __init__(self, path):
		self.path = path
	__repr__ = wash.util.standard_repr
	
	def getAppName(self):
		"""
		Parses the application name out of the EAR file's application.xml file.
		"""
		return _extractEarInfo(self.path, 'display-name')

	def extractSecurityRoleBindings(self):
		"""
		Returns a list of SecurityRoleBinding objects.
		"""
		bindings = []
		for record in _parseAdminAppTaskInfoRecords(AdminApp.taskInfo(self.path, 'MapRolesToUsers').splitlines()):
			binding = SecurityRoleBinding(
					name = record['Role'],
					everyone = record['Everyone?'],
					allAuthenticated = record['All authenticated?'],
					users = _splitOrEmpty(record['Mapped users']),
					groups = _splitOrEmpty(record['Mapped groups']),
					allAuthenticatedInTrustedRealm = record['All authenticated in trusted realms?'],
					userAccessIds = _splitOrEmpty(record['Mapped users access ids']),
					groupAccessIds = _splitOrEmpty(record['Mapped groups access ids']))
			bindings.append(binding)
		return bindings

	def extractModuleServerBindings(self):
		"""
		Returns a list of ModuleServerBinding objects.
		"""
		bindings = []
		for record in _parseAdminAppTaskInfoRecords(AdminApp.taskInfo(self.path, 'MapModulesToServers').splitlines()):
			binding = ModuleServerBinding(
					module = record['Module'],
					uri = record['URI'],
					servers = _splitOrEmpty(record['Server']))
			bindings.append(binding)
		return bindings

	def extractModuleJndiBindings(self):
		"""
		Returns a list of JndiBinding objects.
		"""
		bindings = []
		for record in _parseAdminAppTaskInfoRecords(AdminApp.taskInfo(self.path, 'MapResRefToEJB').splitlines()):
			binding = JndiBinding(
					module = record['Module'],
					ejb = record['EJB'],
					uri = record['URI'],
					ref = record['Resource Reference'],
					type = record['Resource type'],
					target = record['Target Resource JNDI Name'])
			bindings.append(binding)
		return bindings
#

class ModuleServerBinding(wash.util.SelfHelp):
	"""
	Handles the specifics of mapping an application module to WebSphere servers.
	"""

	def __init__(self, module, uri, servers):
		self.module = module
		self.uri = uri
		self.servers = servers
	__repr__ = wash.util.standard_repr

	def forAdminAppEdit(self):
		"""
		Returns a string ready for use in AdminApp.edit(...).

		Return form is something like {
			[ "Module Name" uri server+server+server ]
		}, square brackets are included.
		"""
		return ('[ "%s" %s %s ]' % (self.module, self.uri, "+".join(self.servers)))
#

class JndiBinding(wash.util.SelfHelp):
	"""
	Handles the specifics of mapping an application's modules' JNDI references.
	"""

	def __init__(self, module, ejb, uri, ref, type, target):
		self.module = module
		self.ejb = ejb
		self.uri = uri
		self.ref = ref
		self.type = type
		self.target = target
	__repr__ = wash.util.standard_repr

	def forAdminAppEdit(self):
		"""
		Returns a string ready for use in AdminApp.edit(...).

		Provides something like {
			[ "Module" "EJB" uri resource-reference resource-type target-jndi-name ]
		}, square brackets included.
		"""
		return ('[ "%s" "%s" %s %s %s %s ]' % (
				self.module, self.ejb, self.uri, self.ref, self.type, self.target))
#

class SecurityRoleBinding(wash.util.SelfHelp):
	"""
	Handles the specifics of mapping users and groups.
	"""
	def __init__(self, name,
			everyone, allAuthenticated,
			users, groups,
			allAuthenticatedInTrustedRealm,
			userAccessIds, groupAccessIds):
		"""
		Creates a new security role mapping.

		For this class (until I get around to cleaning it up) the "boolean" options are either
		'AppDeploymentOption.No' or 'AppDeploymentOption.Yes'.

		User and group access IDs differ from the users and groups in that the access IDs
		uniquely identify users across all realms.

		This mapping exposes the following fields:

		name: the role name.
		everyone: boolean indicating if this role is mapped to everyone.
		allAuthenticated: boolean indicating if this role is mapped to all authenticated users.
		users: list of user IDs this role is mapped to.
		groups: list of group IDs this role is mapped to.
		allAuthenticatedInTrustedRealm: boolean indicating this role is mapped to every principal authenticated in a trusted realm.
		userAccessIds: list of user access IDs this role is mapped to.
		groupAccessIds: list of group access IDs this role is mapped to.
		"""
		self.name = name
		self.everyone = everyone
		self.allAuthenticated = allAuthenticated
		self.users = users
		self.groups = groups
		self.allAuthenticatedInTrustedRealm = allAuthenticatedInTrustedRealm
		self.userAccessIds = userAccessIds
		self.groupAccessIds = groupAccessIds
	__repr__ = wash.util.standard_repr

	def forAdminAppEdit(self):
		"""
		Returns a string ready for use in AdminApp.edit(...):
			roles = [ ... a list of these objects ... ]
			AdminApp.edit(appName, '-MapRolesToUsers [%s]' % (
					"".join([ role.forAdminAppEdit() for role in roles ])))
		Return form is something like {
			[
				role
				AppDeploymentOption.No
				AppDeploymentOption.No
				"users"
				"groups"
				AppDeploymentOption.No
				"user-access-ids"
				"group-access-ids"
			]
		}, square brackets are included, newlines omitted.
		"""
		return ('[ %s %s %s "%s" "%s" %s "%s" "%s" ]'
				% ( self.name,
						self.everyone, self.allAuthenticated,
						"+".join(self.users), "+".join(self.groups),
						self.allAuthenticatedInTrustedRealm,
						"+".join(self.userAccessIds), "+".join(self.groupAccessIds)))
#

class Container(wash.util.SelfHelp):
	"""
	Containers form the deployment target of an application.

	There are two main types of containers:
	- DeploymentManager: manages a group of one or more
	**AppServer**s, grouped into zero or more **ServerCluster**s.
	- SingleServer: A single, standalone app server.

	Each container should be able to determine if the currently connected
	wsadmin process "matches" it. This forms the basis for selecting
	the appropriate configuration "profile".

	Each container type has the ability to install and remove applications.
	"""

	def __init__(self, name, applications = []):
		"""
		Creates an new container descriptor.

		The name defaults to the current cell name.
		"""
		self.name = (name or AdminControl.getCell())
		self.applications = applications[:]
	__repr__ = wash.util.standard_repr

	def uninstallApplication(self, appName):
		"""
		Removes the specified application.

		This method has a default implementation which should suffice
		for any app container, but it may be overridden by subtypes.

		If the application is not installed then this method does nothing.

		Subtypes may also wish to wrap this method to stop the
		application before removing it, or remember the state.
		"""
		if (appName in AdminApp.list().splitlines()):
			wash.util.trace("Uninstalling %s" % appName)
			AdminApp.uninstall(appName)
	
	def matchesCurrentEnvironment(self):
		"""
		Returns True if the current environment is a match for this container.
		
		Must be implemented by the specific container types. This implementation returns False.
		"""
		return False
#

class DeploymentManager(Container):
	"""
	A type of container that manages one or more application containers grouped into zero or more clusters.
	"""

	__init__ = Container.__init__
	__repr__ = wash.util.standard_repr

	def matchesCurrentEnvironment(self):
		"""
		Returns True if we are connected to a Cell with the same name as this Container config.
		"""
		try:
			return (AdminControl.getCell() == self.name and True or False)
		except:
			wash.util.trace("ERROR getting cell name: %s" % "; ".join(sys.exc_info()[0:1]))
		return False

	def installApplication(self, earFile, appName):
		"""
		Installs the ear file into this container.
		"""
		wash.util.trace("Looking up deployment configuration for %s" % appName)
		appConfigs = [ appConfig for appConfig in self.applications if appConfig.name == appName ]
		if (len(appConfigs) != 1):
			raise ValueError('Bad Configuration; found %d application configurations for "%s" in <%s: %s>: %s' % (
					len(appConfigs), self.__class__.__name__, self.name, "; ".join([ appConfig.name for appConfig in appConfigs ])))
		appConfig = appConfigs[0]
		wash.util.trace("Found application config: %s" % appConfig)
		appConfig.install(earFile)
#

class Application(wash.util.SelfHelp):
	"""
	Declares the application to deploy.
	"""

	def __init__(self, name, standardOptions = [], classLoader = None, securityMapping = None, modules = []):
		"""
		Each **container** declaration defines target configurations for the given application.
		"""
		self.name = name
		self.standardOptions = standardOptions[:]
		self.classLoader = classLoader
		self.securityMapping = securityMapping
		self.modules = modules[:]
	__repr__ = wash.util.standard_repr
	
	def install(self, earFile):
		"""
		Installs this application into the current environment.
		"""
		# don't forget to copy - we need to add the app's name
		options = self.standardOptions[:]
		options.append('appname %s' % self.name)
		wash.util.trace("Installing %s" % self.name)
		AdminApp.install(earFile.path, '[  -'+" -".join(options)+' ]' )
		
		self.applyConfiguration(earFile)
	
	def applyConfiguration(self, earFile):
		"""
		Apply this application configuration to the current installation.
		"""
		if (self.classLoader):
			self.classLoader.apply(self.name)

		if (self.securityMapping):
			self.securityMapping.apply(earFile, self)

		for module in self.modules:
			module.apply(earFile, self)
#

class ApplicationClassLoader(wash.util.SelfHelp):
	"""
	Configures the class loader for an application in a container.
	"""
	# FIXME: bad name?

	def __init__(self, single = True, parentLast = False):
		self.single = single
		self.parentLast = parentLast
	__repr__ = wash.util.standard_repr

	def apply(self, appName):
		wash.util.trace("Applying classLoader config: %s" % self)
		deployedObject = wash.state.ConfigType('Deployment').get(appName).child('deployedObject')
		deployedObject.attr.warClassLoaderPolicy = (self.single and 'SINGLE' or 'MULTIPLE')
		deployedObject.pushChanges()
		classLoader = deployedObject.child('classloader')
		classLoader.attr.mode = (self.parentLast and 'PARENT_LAST' or 'PARENT_FIRST')
		classLoader.pushChanges()
#

class AllModules(wash.util.SelfHelp):
	"""
	Acts as a simple config or a catch all for modules not specified.
	"""

	def __init__(self, appServers = [], webServers = [], classLoader = None, jndiRefs = []):
		self.appServers = appServers[:]
		self.webServers = webServers[:]
		self.classLoader = classLoader
		self.jndiRefs = jndiRefs[:]
	__repr__ = wash.util.standard_repr

	def apply(self, earFile, appConfig):
		"""
		Configures the modules.
		"""
		if (self.classLoader):
			for module in wash.state.ConfigType('Deployment').get(appConfig.name).child('deployedObject').children('modules'):
				self.classLoader.apply(module)

		serverMappings = earFile.extractModuleServerBindings()
		for mapping in serverMappings:
			mapping.servers = []
			mapping.servers.extend([ server.mappingId() for server in self.appServers ])
			mapping.servers.extend([ server.mappingId() for server in self.webServers ])
		wash.util.trace("Mapping modules to servers %s" % repr(serverMappings))
		AdminApp.edit(appConfig.name, '-MapModulesToServers [%s]' % (
				"".join([ mapping.forAdminAppEdit() for mapping in serverMappings ])))

		for jndiRef in self.jndiRefs:
			jndiRef.apply(earFile, appConfig)
#

class AppServer(wash.util.SelfHelp):
	"""
	A single application server; not in a cluster.
	"""

	def __init__(self, name):
		self.name = name
	__repr__ = wash.util.standard_repr

	def mappingId(self):
		"""
		Returns a mapping ID suitable for use with AdminApp.edit()
		"""
		server = wash.state.ConfigType('Server').get(self.name)
		parent = server.parent()
		while (parent.configType().name != 'Cell'):
			parent = parent.parent()
		cell = parent
		return ("WebSphere:cell=%s,server=%s" % (cell.attr.name, server.attr.name))
#

class ServerCluster(wash.util.SelfHelp):
	"""
	A group of application servers.
	"""

	def __init__(self, name):
		self.name = name
	__repr__ = wash.util.standard_repr

	def mappingId(self):
		"""
		Returns a mapping ID suitable for use with AdminApp.edit()
		"""
		server = wash.state.ConfigType('ServerCluster').get(self.name)
		parent = server.parent()
		while (parent.configType().name != 'Cell'):
			parent = parent.parent()
		cell = parent
		return ("WebSphere:cell=%s,cluster=%s" % (cell.attr.name, server.attr.name))
#

class WebServer(wash.util.SelfHelp):
	"""
	A simple HTTP server.
	"""

	def __init__(self, name):
		self.name = name
	__repr__ = wash.util.standard_repr

	def mappingId(self):
		"""
		Returns a mapping ID suitable for use with AdminApp.edit()
		"""
		server = wash.state.ConfigType('WebServer').get(self.name)
		parent = server.parent()
		while (parent.configType().name != 'Cell'):
			parent = parent.parent()
		cell = parent
		return ("WebSphere:cell=%s,server=%s" % (cell.attr.name, server.attr.name))
#

class ModuleClassLoader(wash.util.SelfHelp):
	"""
	Defines the settings for a module's class loader.
	"""

	def __init__(self, parentLast = False):
		"""
		Initializes this class loader setting to use Parent First semantics.
		"""
		self.parentLast = parentLast
	__repr__ = wash.util.standard_repr

	def apply(self, module):
		"""
		Applies this configuration to the module.

		The module argument is the Config object for the module.
		"""
		mode = (self.parentLast and "PARENT_LAST" or "PARENT_FIRST")
		wash.util.trace("Setting module [%s] class loader mode to '%s'" % (module.attr.uri, mode))
		module.attr.classloaderMode = 'PARENT_LAST'
		module.pushChanges()
		moduleClassLoader = module.child('classloader');
		moduleClassLoader.attr.mode = 'PARENT_LAST'
		moduleClassLoader.pushChanges()
#

class PrefixedLdapRoleMapping(wash.util.SelfHelp):
	"""
	Simple LDAP role mapper that searches for the roles in LDAP using a prefix.
	"""
	# FIXME: bad description

	def __init__(self, securityDomain, rolePrefix):
		self.securityDomain = securityDomain
		self.rolePrefix = rolePrefix
	__repr__ = wash.util.standard_repr

	def apply(self, earFile, appConfig):
		"""
		Applies the security mapping.
		"""
		wash.util.trace("Configuring security role mappings for security domain {%s}..." % self.securityDomain)
		"""
		OK; another fucked up piece of shit from IBM:
			wsadmin>print AdminTask.listRegistryGroups('-securityDomainName vm-ldap -groupFilter GBL_BB_* -displayAccessIds true')
			[[name [cn=GBL_BB_GOD,...,dc=fmh]] [accessId [group:gw.vm.home.fmh:389/cn=GBL_BB_GOD,...,dc=fmh]] ]
			[[name cn=GBL_BB_NOOB,...,dc=fmh] [accessId group:gw.vm.home.fmh:389/cn=GBL_BB_NOOB,...,dc=fmh] ]
		"""
		securityGroups = {}
		registryGroups = AdminTask.listRegistryGroups(
				'-securityDomainName %s -groupFilter %s* -displayAccessIds true' % (
						self.securityDomain, self.rolePrefix)).splitlines()
		wash.util.trace("Found %i groups in the %s security domain" % (len(registryGroups), self.securityDomain))
		for item in registryGroups:
			# starts with '[['; func requires '[ ['
			securityGroup = wash.util.propsToDictionary('[ ' + item[1:])
			# parse out the name which is the 'cn' part of the DN, which is also conveniently first
			name = securityGroup['name'].split(',',1)[0].split('=',1)[1].strip()
			# rename then 'name' in the result to 'dn'
			securityGroups[name] = { 'dn': securityGroup['name'], 'accessId': securityGroup['accessId'] }
		securityRolesToMap = []
		earSecurityRoles = earFile.extractSecurityRoleBindings()
		wash.util.trace("Found %i security roles in app ear for %s"  % (len(earSecurityRoles), appConfig.name))
		for securityRole in earSecurityRoles:
			if (securityGroups.has_key(self.rolePrefix + securityRole.name)):
				wash.util.trace(" ... Found match for %s" % securityRole.name)
				securityRolesToMap.append(securityRole)
				securityGroup = securityGroups[self.rolePrefix + securityRole.name]
				securityRole.groups.append(securityGroup['dn'])
				securityRole.groupAccessIds.append(securityGroup['accessId'])
		wash.util.trace("Mapping %i groups in the %s user registry" % (len(securityRolesToMap), self.securityDomain))
		AdminApp.edit(appConfig.name, '-MapRolesToUsers [%s]' % (
				"".join([ role.forAdminAppEdit() for role in securityRolesToMap ])))
#

class SimpleJndiMapping(wash.util.SelfHelp):
	"""
	A JNDI binding specification that simply maps a referenced name to a target.
	"""

	def __init__(self, ref, target):
		"""
		Creates a new JNDI binding specification.

		The ref is the JNDI name that the module will request;
		the target is the JNDI name that the app container knows.
		"""
		self.ref = ref
		self.target = target
	__repr__ = wash.util.standard_repr

	def apply(self, earFile, appConfig):
		"""
		Applies this mapping.
		"""
		jndiMappings = [ mapping for mapping in earFile.extractModuleJndiBindings() if mapping.ref == self.ref ]
		for mapping in jndiMappings:
			mapping.target = self.target
		wash.util.trace("Mapping JNDI references: %s" % repr(jndiMappings))
		AdminApp.edit(appConfig.name, '-MapResRefToEJB [%s]' % (
				"".join([mapping.forAdminAppEdit() for mapping in jndiMappings])))
#

def _parseAdminAppTaskInfoRecords(lines, headerLine = 'The current contents of the task after running default bindings are:'):
	"""
	Returns a list of dictionaries representing the line-oriented "records" as obtained from AdminApp.taskInfo()'s examination of the EAR file.

	This assumes that line-oriented records are separated by at least one blank line
	and each record "field" has the form "key: value".
	"""
	records = []
	inHeader = True
	for line in lines:
		if (inHeader):
			if (line == headerLine):
				inHeader = False
				record = {}
			continue

		if (line and line.strip()):
			line = line.strip()
			# check to see if it is a "key: value" form; the value is optional...
			if (line.find(':') < 1):
				continue
			# Guards against multiple blank lines: if the record is "new",
			# and the line is a KV pair (by virtue of hitting this line);
			# append this record to the list
			if (not record):
				records.append(record)
			(key, value) = [ item.strip() for item in line.split(':',1) ]
			record[key] = value
		elif (record):
			# If the record is not "new" then this is the first blank line;
			# create a new record
			record = {}
	return records
#

def _splitOrEmpty(string, delim = '+'):
	"""
	Returns an empty list if the string is empty; otherwise a list containing stripped components separated by the delimiter.
	"""
	if (string and string.strip()):
		return [ item.strip() for item in string.split(delim) ]
	else:
		return []
#

def _extractEarInfo(earFile, xpath):
	"""
	Pulls information out of an EAR's application.xml file.
	"""
	zipfile = java.util.zip.ZipFile(earFile)
	appXmlDoc = wash.util.getXmlDoc(zipfile.getInputStream(zipfile.getEntry('META-INF/application.xml')))
	appXmlDocRoot = appXmlDoc.documentElement
	return javax.xml.xpath.XPathFactory.newInstance().newXPath().compile(
			'/application/' + xpath).evaluate(appXmlDocRoot)
#

## EOF
