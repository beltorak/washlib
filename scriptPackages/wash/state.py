# -*- coding: utf-8 -*-

###############################################################################
# Copyright 2012 Trevor "beltorak" Torrez
# under The Apache License, version 2.0.
#
# See http://www.apache.org/licenses/LICENSE-2.0
# (or the file COPYING in the root of this dist)
# for details.
###############################################################################


"""
Provides a pythonic view of the runtime and static state of WebSphere.

This information is gathered primarily from the wsadmin config and control objects.
"""

import AdminUtilities
import AdminConfig

import com.ibm.ws.scripting.ScriptingException as ScriptingException

from wash.compat import *

import wash.util

class BadContainmentSpecifier(ValueError):
	"""
	Indicates that the containment specifier is not valid;
	it does not resolve to exactly one config element.
	"""
	def __init__(self, spec, resolved = []):
		ValueError.__init__(self, '"%s" resolves to %d config ids%s' % (
				spec, len(resolved), (resolved and ["\n  " + "\n  ".join(resolved)] or [""])[0]))
#

class Config(wash.util.SelfHelp):
	"""
	Wrapper class for wsadmin configuration items.
	"""

	def __init__(self, spec):
		"""
		Creates a config helper for the given containment or config id.

		The spec must conform to a containment specifier (a series of
		'/type:name/type:name/' where the 'name' is optional)
		or a config id (something like name(path/to/config) or
		"name with spaces(path/to/config)" - the quotes are optional).
		"""
		if (not spec):
			raise ValueError("No spec given")
		
		if(spec[0] + spec[-1] == '//'):
			self.containment = spec
			configId = AdminConfig.getid(spec).splitlines()
			if (len(configId) != 1):
				raise BadContainmentSpecifier(spec, configId)
			self._configId = configId[0]
		else:
			self._configId = spec
		
		self.reload()


	def __str__(self):
		return (self.name() and [ repr(self.name()) + ' ' ] or [ "" ])[0] + ("[%s] " % self.configType()) + self.configId()
	def __repr__(self):
		return 'Config(%s)' % repr(self.configId())

	def name(self):
		"""
		Returns the name of this config item if the attributes define a name; otherwise returns the empty string
		"""
		return (self.attr.has_key('name') and [ self.attr['name'] ] or [ "" ])[0]


	def configId(self):
		"""
		Returns the configuration ID
		"""
		return self._configId

	def objectName(self):
		"""
		Returns the MBean Object Name for this configuration element if it exists
		"""
		if (not self.__dict__.has_key('_objectName')):
			self._objectName = (AdminConfig.getObjectName(self.configId()) or None)
		return self._objectName

	def mbean(self):
		"""
		Returns an object to interact with the WebSphere MBean.

		Raises ValueError if there is no MBean for this Config object (that is,
		if .objectName() returns None).
		"""
		if (self.objectName()):
			return WashMBean(self.objectName())
		raise ValueError("No JMX MBean for %s" % self)

	def configType(self):
		"""
		Returns ConfigType object representing the type of this config element.
		"""
		if (not self.__dict__.has_key('_configType')):
			self._configType = ConfigType(AdminConfig.getObjectType(self.configId()))
		return self._configType

	def child(self, attrName):
		"""
		Returns the Config of a child specified by the attribute name.

		The attribute value must be a string; if the attribute value is None or the
		attribute is not present then this raises a ValueError.
		"""
		if (self.attr.has_key(attrName) and type(self.attr[attrName]) == type("")):
			return Config(self.attr[attrName])
		raise ValueError(
				"Attribute %s value { %s } is not a valid config id"
				% (attrName, repr(self.attr[attrName])))

	def children(self, attrName):
		"""
		Returns a list of child Config objects from the list of config ids in one of the attributes.

		Empty attributes (or those not present) are returned as an empty list.
		If the value is not a config id (or a list of them) then this throws an error.
		"""
		children = []
		if (self.attr.has_key(attrName) and self.attr[attrName]):
			if (type(self.attr[attrName]) == type("")):
				childIds = [ self.attr[attrName] ]
			else:
				childIds = self.attr[attrName][:]
			for child in childIds:
				children.append(Config(child))
		return children

	def parent(self):
		"""
		Attempts to find the parent configuration object of this item.

		This is potentially a costly operation as we have to search through
		the children of each possible parent type using AdminConfig.getid().
		Returns None if we cannot find anything. May throw an error besides.
		"""
		for parentType in self.configType().parents():
			for parent in parentType.find():
				for posChild in AdminConfig.getid('/%s:%s/%s:/' % (parentType.name, parent.name(), self.configType().name)).splitlines():
					if (posChild == self.configId()):
						return parent
		return None

	def getChanges(self):
		"""
		Returns a list of changes that will be written to the configuration.

		Each item is a 3-tuple: (action, name, value) where
			'action' is
				'A' for adding a new attribute,
				'M' for changing an attribute value,
				'D' for deleting an attribute,
			'name' is the attribute name,
			'value' is the new value.
		For deletions the value is always None.
		"""
		changes = []
		for (key, val) in self.attr.items():
			if (not self._orig_attrs.has_key(key)):
				changes.append( ('A', key, val) )
			elif (val != self._orig_attrs[key]):
				changes.append( ('M', key, val) )

		for key in self._orig_attrs.keys():
			if (key not in self.attr.keys()):
				changes.append( ('D', key, None) )

		return changes

	def pushChanges(self):
		"""
		Writes the pending changes to the configuration.

		This does not do AdminConfig.save().
		"""
		mods = []
		drops = []
		for (action, key, value) in self.getChanges():
			if (action == 'D'):
				drops.append(key)
			else:
				if (self.configType().typeof(key) == bool):
					mods.append([key, (`self.attr[key]`).lower()])
				else:
					mods.append([key, self.attr[key]])
		try:
			if drops:
				AdminConfig.unsetAttributes(self.configId(), drops)
			if mods:
				AdminConfig.modify(self.configId(), mods)
		finally:
			self.reload()

	def reload(self):
		"""
		Loads the configuration from WebSphere. Discards any pending changes.
		"""
		# FIXME: this does not properly handle newlines in attribute values; thanks IBM :-/
		self._orig_attrs = wash.util.AttrDict()
		for attrString in AdminConfig.show(self.configId()).splitlines():
			if (attrString == '[]'):
				self._orig_attrs[key] = []
			elif ((attrString[0] + attrString[-1]) == '[]'):
				(key, val) = attrString[1:-1].split(' ',1)
				if (val is None or val == '[]'):
					self._orig_attrs[key] = None
				elif ((val[0] + val[-1]) == '[]'):
					self._orig_attrs[key] = AdminUtilities.convertToList(val)
				elif ((val[0] + val[-1]) == '""'):
					self._orig_attrs[key] = val[1:-1]
				else:
					if (self.configType().typeof(key) == bool):
						self._orig_attrs[key] = bool(val)
					elif (self.configType().typeof(key) == type(1)):
						self._orig_attrs[key] = int(val)
					else:
						self._orig_attrs[key] = val
			else:
				print "** ERROR: Config.reload(configId=%s): attrString={%s}" % (self.configId(), attrString)
		self.attr = self._orig_attrs.copy()
#

class ConfigChanges(wash.util.SelfHelp):
	"""
	A type that provides for tracking changes across config items and allows for autosync.

	The autoSync flag defaults to True and synchronizes all the nodes when the changes are
	pushed to WebSphere.
	"""

	def __init__(self):
		self.autoSync = True
		self._changedConfigs = {}

	def track(self, config):
		"""
		Tracks the config object for changes.

		Returns the config object to allow easy invocation.
		"""
		self._changedConfigs[config.configId()] = config
		return config

	def query(self):
		"""
		Returns a dict of all the changes pending to be written to WebSphere configuration
		keyed by the config id.
		"""
		changes = {}
		for config in self._changedConfigs.values():
			changes[config.configId()] = config.getChanges()[:]
		return changes

	def abandon(self):
		"""
		Reverts all stored Config object changes.

		Returns True.
		"""
		for config in self._changedConfigs.values():
			config.dropChanges()
		self._changedConfigs = {}
		return True

	def save(self):
		"""
		Pushes all changes to WebSphere.

		If self.autoSync is true then this synchronizes the changes across nodes.
		"""
		if (not self._changedConfigs):
			trace("Nothing to save to WebSphere")
			return

		trace("Pushing configuration changes to WebSphere")
		for config in self._changedConfigs.values():
			trace(" ... %s" % (config.name() or config.configId()))
			config.pushChanges()
		self._changedConfigs = {}

		trace("Saving changes to WebSphere configuration...")
		AdminConfig.save()
		if (self.autoSync):
			wash.util.syncNodes()
#

class ConfigType(wash.util.SelfHelp):
	"""
	A descriptive class providing information about the configuration item type.

	This class has no direct analog within the WebSphere scripting libararies,
	but it provides access to several AdminConfig methods that are not
	instance-specific; that is all the AdminConfig methods that take a
	configuration type instead of a containment path or config ID.
	"""

	def __init__(self, name):
		"""
		Initializes a config element type.

		The name argument must correspond to a valid value taken from
		AdminConfig.types().
		"""
		self.name = name

	def __str__(self):
		return self.name
	def __repr__(self):
		return 'ConfigType(%s)' % repr(self.name)

	def parents(self):
		"""
		Returns a list of the config types that can be a parent of this type.

		NOTE: sometimes websphere returns (yes, returns) an error message indicating
		that the parents command cannot be used; in that case this method
		raises a com.ibm.ws.scripting.ScriptingException
		"""
		parentNames = AdminConfig.parents(self.name)
		if (parentNames.startswith('WASX7351I: The parents command cannot be used')):
			raise ScriptingException(parentNames)
		parents = []
		for parentName in parentNames.splitlines():
			parent = ConfigType(parentName)
			parents.append(parent)
		return parents

	def defaults(self):
		"""
		Returns a dictionary of default values for this configuration type, keyed by the attribute name.

		The value is an TypedValue instance.

		These attributes include all those returned by self.required().
		There is some type translation to convert strings to thier native
		Jython equivalents, such as for booleans (and more to come). As such
		the types returned from here represent the canonical types of the
		config attributes.
		"""
		defaults = {}
		# first line is a 'header'; :-/ thanks IBM :-/
		attrs = AdminConfig.defaults(self.name).splitlines()[1:]
		# lines are like 'name                      type               (maybe a value)'; :-/ thanks IBM :-/
		for line in attrs:
			items = line.split()
			(name, type, value) = (items[0], items[1], (len(items)==3 and [items[2]] or [None])[0])
			defaults[name] = wash.util.TypedValue(type, value)
		return defaults

	def required(self):
		"""
		Returns a dictionary of the required attributes for this config type, keyed by the attribute name.
		"""
		required = {}
		# first line is a 'header'; :-/ thanks IBM :-/
		attrs = AdminConfig.required(self.name).splitlines()[1:]
		# lines are like 'name                   type'; :-/ thanks IBM :-/
		for (name, type) in [ (item[0], item[1]) for item in [ line.split() for line in attrs ] ]:
			required[name] = self.typeof(name)
		return required

	def typeof(self, attr):
		"""
		Returns the canonical type of the attribute value.
		"""
		return self.defaults()[attr].type

	def get(self, name):
		"""
		Returns a Config object for the given name.

		For example,
		ds = ConfigType('DataSource').get('project_ds')
		is the same as
		ds = Config('/DataSource:project_ds/')
		"""
		return Config('/%s:%s/' % (self.name, name))

	def find(self, scope = None):
		"""
		Performs a query using AdminConfig.list(). The scope is optional.

		Returns a list Config objects of this type in the specified scope.
		"""
		results = []
		if (scope):
			configIds = AdminConfig.list(self.name, scope).splitlines()
		else:
			configIds = AdminConfig.list(self.name).splitlines()
		for configId in configIds:
			results.append(Config(configId))
		return results

	def create(self, *args, **kwargs):
		"""
		TODO: provide a means to create new config elements.
		"""
		raise NotImplemented
#

class WashMBeanFeatureType(wash.util.SelfHelp):
	"""
	Simple object that combines a name, type, and access method.
	"""
	def __init__(self, name, typeName, access):
		"""
		Creates a new feature descriptor.
		"""
		self.name = name
		self.typeName = typeName
		self.access = access

	def __str__(self):
		return repr(self)
	def __repr__(self):
		return "%s(name=%s,typeName=%s,access=%s)" % (
				self.__class__.__name__,
				repr(self.name),
				repr(self.typeName),
				repr(self.access))
#

class WashMBean(wash.util.SelfHelp):
	"""
	A wrapper class to assist in interacting with the MBeans exposed by WebSphere.
	"""

	def __init__(self, objectName):
		"""
		Creates a new MBean wrapper.

		The object name must resolve to
		exactly one MBean via AdminControl.queryNames().
		"""
		objectNames = wash.util.mbeanResultToList(AdminControl.queryNames(objectName))
		if (len(objectNames) != 1):
			raise ValueError("Object Name must resolve to exactly 1 mbean; found %d mbeans for {%s}" % (len(objectNames), objectName))
		self._objectName = objectNames[0]
		self._domain = self.objectName().split(':',1)[0]
		self._nameComponents = wash.util.AttrDict()
		for nameComponent in self.objectName().split(':',1)[1].split(','):
			(key, value) = nameComponent.split('=',1)
			self._nameComponents[key] = value

	def __str__(self):
		return repr(self)
	def __repr__(self):
		return "WashMBean(%s)" % repr(self.objectName())

	def objectName(self):
		"""
		Returns the canonical object name of this MBean.
		"""
		return self._objectName

	def domain(self):
		"""
		Returns the MBean domain.

		This is almost always 'WebSphere'.
		"""
		return self._domain

	def nameComponents(self):
		"""
		Returns a copy of a dictionary that is the MBean Object Name broken down into components.
		"""
		return self.nameComponents.copy()

	def config(self):
		"""
		Returns the Config item for this MBean if it exists.
		"""
		configId = AdminControl.getConfigId(self.objectName())
		return (configId and Config(configId) or None)

	def attrTypes(self):
		"""
		Returns a dictionary of WashMBeanFeeatureType objects describing the attributes of this MBean, keyed by name.
		"""
		# first line is a header; thanks IBM :-/
		attrTypes = wash.util.AttrDict()
		for line in Help.attributes(self.objectName()).splitlines()[1:]:
			items = line.split()
			attrType = WashMBeanFeatureType(items[0], items[1], items[2])
			attrTypes[attrType.name] = attrType
		return attrTypes
	
	def invoke(self, operation, types = [], *arguments):
		"""
		Invokes an operation on this MBean.
		"""
		if (types):
			return AdminControl.invoke(self.objectName(), operation, " ".join(arguments), "[%s]" % ",".join(types))
		else:
			return AdminControl.invoke(self.objectName(), operation, " ".join(arguments))
#

def getApplicationState(appName):
	"""
	Returns a string corresponding to the application state;
	'Absent', 'Installed', or 'Running'
	"""
	if appName in AdminApp.list().splitlines():
		if (len(AdminControl.queryNames('type=Application,name=%s,*' % appName).splitlines())):
			return 'Running'
		else:
			return "Installed"
	else:
		return "Absent"
#

def getServersForApplication(appName):
	"""
	Returns a list of Config objects for the containers that currently host an application.
	"""
	targets = ConfigType('Deployment').get(appName).children('deploymentTargets')
	servers = []
	# do not store web servers
	webServers = [ server.name() for server in ConfigType('WebServer').find() ]
	for target in [ target for target in targets if target.name() not in webServers ]:
		# separate servers from clusters
		if (target.attr.has_key('nodeName')):
			servers.append(target)
		else:
			# expand cluster members
			servers.extend([ ConfigType('Server').get(member.attr.memberName) for member in ConfigType('ServerCluster').get(target.name()).children('members') ])
	return servers
#

def stopApplication(appName):
	"""
	Stops an application on all its deployed servers.
	"""
	for manager in [ WashMBean('type=ApplicationManager,process=%s,*' % server.name()) for server in getServersForApplication(appName) ]:
		wash.util.trace("Stopping %s on {%s}" % (appName, manager.objectName()))
		manager.invoke('stopApplication', [], appName)

def startApplication(appName):
	"""
	Starts an application on all its deployed servers.
	"""
	for manager in [ WashMBean('type=ApplicationManager,process=%s,*' % server.name()) for server in getServersForApplication(appName) ]:
		wash.util.trace("Starting %s on {%s}" % (appName, manager.objectName()))
		manager.invoke('startApplication', [], appName)

## EOF
