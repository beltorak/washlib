# -*- coding: utf-8 -*-

###############################################################################
# Copyright 2012 Trevor "beltorak" Torrez
# under The Apache License, version 2.0.
#
# See http://www.apache.org/licenses/LICENSE-2.0
# (or the file COPYING in the root of this dist)
# for details.
###############################################################################


"""
Some basic checks about how wsadmin jython imports work.

This module verifies that the wsadminobjects.py scriptLibrary does indeed make
the five god objects available.
"""

import AdminConfig
print(AdminConfig.help())


## EOF
