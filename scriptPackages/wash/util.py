# -*- coding: utf-8 -*-

###############################################################################
# Copyright 2012 Trevor "beltorak" Torrez
# under The Apache License, version 2.0.
#
# See http://www.apache.org/licenses/LICENSE-2.0
# (or the file COPYING in the root of this dist)
# for details.
###############################################################################


"""
Provides basic wsadmin and jython utility functions and classes.
"""

import sys
import AdminControl

import java
import javax.xml.parsers.DocumentBuilderFactory

from wash.compat import *


_TRACE_ENABLED = True
def traceOn():
	_TRACE_ENABLED = True
def traceOff():
	_TRACE_ENABLED = False

_TRACE_HEADER = " ** TRACE"
def trace(msg):
	"""
	Prints a message if traceOn() has been called.
	"""
	if (_TRACE_ENABLED):
		print "%s: %s" % (_TRACE_HEADER, msg)
#

def _py_doc_format_as_list(str):
	"""
	Returns the standard formatted __doc__ string as a list of lines.

	Uniform indentation is removed; the indentation is determined by the second
	non blank line. All indentation on the first non-blank line (the summary)
	is removed.

	For a detailed description, see PEP 257, on which this function is stolen.

	"""
	if (not str):
		return ''
	# Convert tabs to spaces following the normal Python rules
	# and split into a list of lines
	lines = str.expandtabs().splitlines()
	# Determine the minimum indentation (first line doesn't count)
	indent = sys.maxint
	for line in lines[1:]:
		stripped = line.lstrip()
		if (stripped):
			indent = min(indent, len(line) - len(stripped))
	# Remove indentation from lines 2+
	trimmed = [lines[0].strip()]
	if (indent < sys.maxint):
		for line in lines[1:]:
			trimmed.append(line[indent:].rstrip())
	# Strip off trailng and leading blank lines
	while (trimmed and not trimmed[-1]):
		trimmed.pop()
	while (trimmed and not trimmed[0]):
		trimmed.pop(0)
	# tada! (except that we return the list for later processing)
	return trimmed
#

def wash_help(obj):
	"""
	Provides help on the class (or class of an instance), or method.

	Example Usage:
		print wash_help(Config)
		print wash_help(Config.child)
	"""
	if (not callable(obj)):
		try:
			obj = obj.__class__
		except:
			pass
	if (callable(obj)):
		is_func = False
		try:
			obj.func_code
			is_func = True
		except AttributeError:
			pass

		is_method = False
		try:
			obj.im_class
			is_method = True
		except AttributeError:
			pass

		if (is_func or is_method):
			return ("\n" + func_help(obj) + "\n").replace("\n","\n    ")
		else:
			return ("\n" + class_help(obj) + "\n").replace("\n","\n    ")
	raise ValueError("%s is not a class or function" % str(obj))
#

def func_help(func):
	"""
	Provides help for a function or a method.

	This returns the docstring for the method
	and the method signature.

	"""
	if (callable(func)):
		func_help = [ func_sig(func) ]
		if (func.__doc__):
			func_help += _py_doc_format_as_list(func.__doc__)
		else:
			func_help.append("No help available.")
		return "\n".join(func_help)
	else:
		raise ValueError("%s is not callable" % str(func))
#

def class_help(cls):
	"""
	Provides help for a class and the __init__ method.
	"""
	class_help = [ "Class %s:" % cls.__name__, "" ]
	class_help += _py_doc_format_as_list(cls.__doc__)
	class_help += [ "", func_sig(cls.__init__).replace('.__init__', '') ]
	class_help += _py_doc_format_as_list(cls.__init__.__doc__)

	methods_help = []
	for (name, item) in [ (name, item) for (name, item) in cls.__dict__.items() if (callable(item) and item.__doc__ and name[0:1] != '_') ]:
		method = getattr(cls, name)
		method_help = func_help(method).splitlines()
		if (len(method_help) > 1):
			methods_help += [ "%s: %s" % (method_help[0], method_help[1]) ]
		else:
			methods_help += method_help
	if (methods_help):
		class_help += [ "", "Methods:" ]
		class_help += methods_help
	return "\n".join(class_help)
#

def func_sig(func):
	"""
	Returns the function signature; <name>(<arg>,<arg>=<default>, ...).
	"""

	try:
		className = func.im_class.__name__
		methodName = func.__name__
		name = "%s.%s" % (className, methodName)
		is_method = True
	except AttributeError:
		is_method = False
		name = func.__name__
	func_code = func.func_code
	args = []
	non_defaults_count = func_code.co_argcount - (func.func_defaults and [len(func.func_defaults)] or [0])[0]
	for i in range(func_code.co_argcount):
		if (i == 0 and is_method):
			continue
		arg = func_code.co_varnames[i]
		if (i >= non_defaults_count):
			default = func.func_defaults[i - non_defaults_count]
			arg = arg + "=" + repr(default)
		args.append(arg)
	return "%s(%s)" % (name, ", ".join(args))
#

def standard_repr(obj, attrNames = None):
	"""
	Provides the standard __repr__ implementation returning something like
		ClassName(attr=value, attr=value)
	"""
	if (attrNames is None):
		attrNames = obj.__class__.__init__.im_func.func_code.co_varnames[1:]
	return "%s(%s)" % (
			obj.__class__.__name__, ", ".join([ "%s=%s" % (attrName, repr(obj.__dict__[attrName])) 
					for attrName in attrNames ]))

def syncNodes():
	"""
	Suncronizes changes across all nodes.
	"""
	nodeSyncs = AdminControl.queryNames('type=NodeSync,*')
	if (nodeSyncs):
		for nodeSync in nodeSyncs.splitlines():
			trace("Syncing %s" % nodeSync)
			AdminControl.invoke(nodeSync, 'sync')
	else:
		trace("No NodeSync MBeans found...")
#

def clearScreen():
	"""
	"" Clears "" the screen (by printing 90 blank lines)
	"""
	for i in range(90):
		print
#

###
# Blatently stolen (and adapted) from
# http://wsadminlib.blogspot.com/2010/07/how-to-handle-object-like-strings.html

def propsToLists(propString):
	"""
	Helper method converts a flat string which looks like a list of properties into a real list of lists.

	Expects input of the form   [ [key0 value0] [key1 value1] ]
	Returns output of the form  [['key0', 'value0'], ['key1', 'value1']]
	Also 'dequotes' the value by removing '['...']'
	Raises a ValueError if the string does not look like a props list.
	"""

	# Check for leading and trailing square brackets.
	if not (propString.startswith( '[ [' ) and propString.endswith( '] ]' )):
		raise ValueError(
			"ERROR: propString does not start and end with two square brackets. propString=%s"
			% ( m, propString, ))

	# Strip off the leading and trailing square brackets.
	propString = propString[3:(len(propString) - 3)]

	# Convert the single long string to a list of strings.
	stringList = propString.split('] [')

	# Convert each enclosed string into a list of strings.
	listList = []
	for prop in stringList:
		(key, value) = prop.split(' ',1)
		if ((value[0] + value[-1]) == '[]'):
			value = value[1:-1]
		listList.append([key.strip(), value.strip()])

	return listList
#

def propsToDictionary(propString):
	"""
	Helper method converts a flat string which looks like a list of properties
	into a dictionary of keys and values.

	Expects input of the form   [ [key0 value0] [key1 value1] ]
	Returns output of the form  { 'key0': 'value0', 'key1': 'value1', }
	Also 'dequotes' the value by removing '['...']'
	Raises a ValueError if the string does not look like a props list.
	"""

	# Check for leading and trailing square brackets.
	if not (propString.startswith( '[ [' ) and propString.endswith( '] ]' )):
		raise ValueError(
			"ERROR: propString does not start and end with two square brackets. propString=%s"
			% propString)

	# Strip off the leading and trailing square brackets.
	propString = propString[3:(len(propString) - 3)]

	# Convert the single long string to a list of strings.
	stringList = propString.split('] [')

	# Transfer each enclosed key and value string into a dictionary
	dict = {}
	for prop in stringList:
		(key, value) = prop.split(' ', 1)
		if (value[0] + value[-1] == '[]'):
			value = value[1:-1].strip()
		dict[key] = value

	return dict
#

#
# end tehft
#

def mbeanResultToList(queryResult, eol_repl = ' '):
	"""
	Takes an MBean query result and parses it into a list of strings.
	This takes care of the problem that some attribute values have embedded newlines.

	The return value is a list of strings. This list (or any string in this list)
	may be provided to mbeanListToNameList
	"""

	listResult = []

	for line in queryResult.split(EOL):
		if (line.find('=') < 0):
			# cannot be a complete entry if there's no '='
			complete = False
		elif ((line.count(',') > 0) and (line.find(',') < line.find('='))):
			# cannot be a complete entry if ',' appears before '='
			complete = False
		else:
			# otherwise it looks like a complete entry
			complete = True

		if (complete):
			listResult.append(line)
		elif (not listResult):
			# uh oh ...
			print "*** ERROR: a non complete entry and there's no previous entry to append it to: %s" % line

	return listResult
#

def mbeanListToNameList(mbeanList):
	"""
	Takes a string from the list of mbeanResultToList and turns it into a list
	of strings having the form
		[ "nameComp1=value1", "nameComp2=value2", ... ]
	If the argument to this function is a list of such strings
	then the return value is also a list of lists:
		[ [ "nameComp1=value1", "nameComp2=value2", ... ], [ "nameComp3=value3", "nameComp4=value4", ... ] ]

	FIXME: this does not take into account that some name component values may have embedded commas :-/
	"""

	result = []
	if (type(mbeanList) is StringType):
		mbeanList = [mbeanList]

	for string in mbeanList:
		result.append(string.split(','))

	if (type(mbeanList) is StringType):
		result = result[0]

	return result
#

def mbeanNameListToDict(mbeanNameList):
	"""
	Takes a list of mbean name components from the result of mbeanListToNameList
	and turns it into a dictionary.

	If the argument to this function is a list of attribute name component lists
	then the return value is a list of dictionaries.

	>>> mbeanNameListToDict(["nameComp1=value1", "nameComp2=value2"])
	{"nameComp1"=>"value1", "nameComp2"=>"value2"}
	>>> mbeanNameListToDict([["nameComp1=value1", "nameComp2=value2"],["nameComp4=value4", "nameComp4=value4"]])
	[{"nameComp1"=>"value1", "nameComp2"=>"value2"}, {"nameComp3"=>"value3", "nameComp4"=>"value4"}]
	"""

	result = []
	if (type(mbeanNameList) is StringType):
		mbeanNameList = [mbeanNameList]

	for mbeanName in mbeanNameList:
		mbeanDict = {}
		for nameComp in mbeanName:
			(key, value) = nameComp.split('=', 1)
			mbeanDict[key] = value
		result.append(mbeanDict)

	if (type(mbeanList) is StringType):
		result = result[0]

	return result
#

class AttrDict:
	"""
	Convenience class so we can abuse the python language to give us
	a javascript-like dict; that is with attributes accessed via the '.'
	operator.

	Note that because of this, all keys must be identifier-like strings.
	"""
	def __init__(self, orig = None):
		"""
		Creates a new AttrDict.

		If **orig** is a dictionary then this
		performs a straight (shallow) copy.

		If **orig** is a two-tuple or a two item list then this initializes
		the dict with one key/value pair.

		If **orig** is a list of arbitrary items then each item in the list
		is imported into the dict as above. Each item therefore must
		be a two-tuple, a two item list, or a dictionary.
		"""
		if (type(orig) == type([]) and len(orig) > 0):
			if (len(orig) == 2 and type(orig[0]) not in [ type([]), type({}), type((None,)) ]):
				# list of two items, single key/value
				self.__dict__[orig[0]] = orig[1]
			else:
				for item in orig:
					if (type(item) == type({}) or isinstance(item, AttrDict)):
						for (key, value) in item.items():
							self.__dict__[key] = value
					elif (type(item) == type([])):
						if (len(item) != 2):
							raise ValueError(
									"List type item %s in orig list is not a two-item list"
									% repr(item))
						self.__dict__[item[0]] = item[1]
					elif (type(item) == type((None,))):
						if (len(item) != 2):
							raise ValueError(
									"List item tuple %s is not a two-tuple" % repr(item))
						self.__dict__[item[0]] = item[1]
					else:
						raise ValueError(
								"Bad item in orig list: (%s: %s)"
								% (type(item), repr(item)))
		elif (type(orig) == type((None,))):
			if (len(orig) != 2):
				raise ValueError("Orig tuple %s is not a two-tuple" % repr(orig))
			self.__dict__[orig[0]] = orig[1]
		elif (type(orig) == type({}) or isinstance(orig, AttrDict)):
			for (key, value) in orig.items():
				self.__dict__[key] = value
		elif (orig is not None):
			raise ValueError(
					"Bad orig value: (%s: %s)" % (type(orig), repr(orig)))

	def __getitem__(self, name):
		return self.__dict__[name]
	def __getattr__(self, name):
		return self.__dict__[name]

	def __setitem__(self, name, val):
		self.__dict__[name] = val
	def __setattr__(self, name, val):
		self.__dict__[name] = val

	def __delitem__(self, name):
		del self.__dict__[name]
	def __delattr__(self, name):
		del self.__dict__[name]

	def __repr__(self):
		return repr(self.__dict__)

	def __len__(self):
		return len(self.__dict__)

	def len(self):
		return self.__dict__.len()

	def iter(self):
		return self.__dict__.iter()

	def iterkeys(self):
		return self.iter()

	def clear(self):
		self.__dict__.clear()

	def copy(self):
		other = AttrDict()
		other.update(self.__dict__)
		return other

	def get(self, key, default = None):
		return self.__dict__.get(key, default)

	def has_key(self, key):
		return self.__dict__.has_key(key)

	def items(self):
		return self.__dict__.items()

	def keys(self):
		return self.__dict__.keys()

	def popitem(self):
		return self.__dict__.popitem()

	def setdefault(self, key, default = None):
		if (self.__dict__.has_key(key)):
			return self.__dict__[key]
		self.__dict__[key] = default
		return default

	def update(self, other):
		for (k, v) in other.items():
			self.__dict__[k] = v
		return None

	def values(self):
		return self.__dict__.values()
#

class SelfHelp:
	"""
	Adds a self describing 'help' method to a class.
	"""
	
	def help(self, method = None):
		"""
		Returns the help string for this class or the specified method.
		"""
		if (method):
			# seems to be no way to get the 'class' without this :-/
			return wash_help(eval("%s.%s" % (self.__class__.__name__, method)))
		else:
			return wash_help(self.__class__)
#

class Immutable:
	"""
	A simple type that prevents the normal modification of attributes.
	"""
	def __delattr__(self, name):
		raise TypeError("Immutable")
	def __setattr__(self, name, value):
		raise TypeError("Immutable")
#

class TypedValue(Immutable):
	"""
	Simple class to combine a type and value into one object.
	"""

	def __init__(self, attrType, value):
		"""
		Creates a new AttrValue.

		This also performs some simple type translations to native
		Jython values. For example, if the attrType argument is
		'boolean' or 'bool' (irrespective of case) then the value
		is converted to a "washcompat.bool".
		"""
		if ((attrType).lower() in [ (type(True)), 'bool', 'boolean' ]):
			self.__dict__['type'] = bool
			if (value is None):
				self.__dict__['value'] = None
			else:
				self.__dict__['value'] = bool(value)
		elif (attrType == 'int' or attrType == type(1)):
				self.__dict__['type'] = type(1)
				if (value is None):
					self.__dict__['value'] = None
				else:
					self.__dict__['value'] = int(value)
		else:
			self.__dict__['type'] = attrType
			self.__dict__['value'] = value

	def __str__(self):
		return '%s (%s)' % (repr(self.value), self.type)
	def __repr__(self):
		return 'AttrValue(%s,%s)' % (repr(self.type), repr(self.value))
#

def getXmlDoc(xmlInputStream):
	"""
	Gets the XML Document object from the XML input stream.

	FIXME: this is not namespace aware, so if the application.xml doc uses a
	namespace, it must use it as the default namespace.

	This closes the stream.
	"""

	try:
		factory = javax.xml.parsers.DocumentBuilderFactory.newInstance()
		factory.setValidating(java.lang.Boolean.FALSE)
		factory.setNamespaceAware(java.lang.Boolean.FALSE)
		return factory.newDocumentBuilder().parse(xmlInputStream)
	finally:
		xmlInputStream.close()
#

## EOF
